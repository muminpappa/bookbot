# BookBot - command line client for the Gerdahallen booking system

## Usecase

For some group trainings, it's difficult to book one of the few places.
That's why I write this small bot to do this for me even when I'm busy
at the time I have to book in order to get a place.

## Prerequisites
You need to be a customer of [Gerdahallen](https://gerdahallen.lu.se) in Lund.

The script expects your login credentials as environment variables:
```bash
export GERDA_USR=me@here.org
export GERDA_PSW=mySecretPassword
```

`bookbot.py` should be easy to run on both Windows, Mac and Linux; all you need is a running Python distribution.

You might need to install the `requests` library if it is not available on your system, either into a virtual environment or globally:
```bash
pip install requests
```

The `setup.sh` script sets up a virtual environment as described above.

The option `--schedule` assumes that the `at` command is available on your system. 

## Adjust the exclude list to you needs

I've excluded some types of grouptrainings, in particulate the Gym sessions that we currently need to book. Adjust the exclude list at the top of the `bookbot.py` script to your needs. If in doubt make it empty; this will merely list all.

```python
exclude_list = []
```
**Hint**: To see which id you want to exclude, use
```bash
curl -s https://gerdahallen.brpsystems.com/brponline/api/ver3/products/groupactivities '
```
or
```bash
curl -s https://gerdahallen.brpsystems.com/brponline/api/ver3/products/groupactivities | jq '.[] | {"id": .id, "name": .name}'
```
which returns a full list of available groupactivities and their names.

## How to run

If you have created a virtual environment before, activate it, e.g.
```bash
. venv/bin/activate
```

Run `python3 bookbot.py -h` to see how to use the script.

### Examples

List the grouptrainings today:
```bash
python3 bookbot.py
```
List the grouptrainings on a specific date:
```bash
python3 bookbot.py -d 2021-03-21
```

Search of any grouptraining containing HIT on a specific date:
```bash
python3 bookbot.py -f HIT
```
*Note: The search is case-insensitive*

Book the grouptraining with the id 123456:
```bash
python3 bookbot.py -b 123456
```

## Known bugs

### Daylight saving time issue

If a booking is scheduled _before_ daylight saving time becomes active for a training
that takes place _after_ daylight saving time becomes active, the scheduled time is off by 1h. 
Even though the `bookableEarliest` response of the backend says it should be possible to book,
the booking fails.
Could be me using `datetime` and `astimezone()` the wrong way,
but it looks more like the json response from the backend indeed is wrong. Seems like the backend
calculates `bookableEarliest = duration.start - 144 hours` which means I should be able to book
at 15:00 on Mar 22, 2021 for a training that takes place at 16:00 on Mar 28, 2021.

## Useful links

### API Documentation

https://leverans09.brpsystems.com/brponline/external/documentation/api3?key=apiintegration#_auth_login 

### How to get a token
    curl -v -X POST -H "Content-Type: application/json" -d '{"username":"me@here.org", "password":"mySecretPassword"}' https://gerdahallen.brpsystems.com/brponline/api/ver3/auth/login

### Which group activities exist
    curl https://gerdahallen.brpsystems.com/brponline/api/ver3/products/groupactivities

### List all your trainings in 2021
    curl -v -X GET -H "Content-Type: application/json" -H "Authorization: Bearer thereallylongtokenyoureceivedavove.butinrealityitisevensomuchlongeryoudontbelieveit" "https://gerdahallen.brpsystems.com/brponline/api/ver3/customers/$username/workouts?period.start=2021-01-01T06%3A00%3A59.999Z&period.end=2021-12-31T21%3A59%3A59.999Z" > all_trainigs_2021.json

User the `username` and `token` returned above. 