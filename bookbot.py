import requests
import json
import os
from datetime import datetime
import argparse
import subprocess

base_url = 'https://gerdahallen.brpsystems.com/brponline/api/ver3'
be_verbose = False
# Adjust the exclude list to your needs, if in doubt, just empty it 
exclude_list = [1296, 1270, 1271, 1272, 1273, 1274, 1275, 1303, 1314, 1238, 1217, 1216, 1215, 631,
628, 559, 557, 24, 25, 26, 27, 32, 36, 44, 45, 53, 53, 54, 56, 57, 58,59,60,61,62,68,138]
#exclude_list = []

# List only trainings that start between these hours
earliest = 5
latest = 19

def verbose_message(r):
    print(json.dumps(r.json(),sort_keys=True, indent=4))
    print(r.status_code)
    print(r.headers)    

def brp_auth():
    headers = {
        'Content-Type':'application/json'
    }
    params = {
        'username':os.environ['GERDA_USR'],
        'password':os.environ['GERDA_PSW']
    }
    r = requests.post(base_url+'/auth/login',json=params, headers=headers)
    try:
        be_verbose and verbose_message(r)
        access_token = r.json()['access_token']
        customerid = r.json()['username']
        print('Token: '+str(access_token))
    except ValueError as err:
        print('Failed to get access_token: '+str(err))
        access_token = None
    return access_token, customerid

def validate_token(access_token, min_ttl = 2000):
    headers = {
        'Content-Type':'application/json',
        'Authorization':'Bearer ' + access_token
    }
    r = requests.post(base_url+'/auth/validate',headers=headers)
    try:
        be_verbose and verbose_message(r)
        ttl = r.json()['expires_in']
    except ValueError as err:
        print('Failed to get access_token: '+str(err))
    return ttl>min_ttl

def get_trainings(start_time, end_time):
    headers = {
        'Content-Type':'application/json'
    }
    params = {
        'period':{
            'start':start_time.isoformat(timespec='milliseconds')+'Z',
            'end':end_time.isoformat(timespec='milliseconds')+'Z'
        }
    }
    r = requests.get(url=base_url+'/businessunits/1/groupactivities',headers=headers,json=params)
    return r

def print_line(g):
    id = g['id']
    name = g['name']
    date = datetime.fromisoformat(g['duration']['start'].replace('Z','+00:00')).astimezone().date().isoformat()
    start = datetime.fromisoformat(g['duration']['start'].replace('Z','+00:00')).astimezone().time().isoformat()
    instructor = len(g['instructors'])>0 and g['instructors'][0]['name'] or ' '
    print(f"{date} \t {start} \t {id} \t {name} ({instructor})")

def search_trainings(searchtext, start_time=datetime.today(), end_time=datetime.today()):
    start_time = start_time.replace(hour=earliest,minute=0,second=0,microsecond=0)
    end_time = end_time.replace(hour=latest,minute=0,second=0,microsecond=0)
    r = get_trainings(start_time,end_time)
    try:
        be_verbose and verbose_message(r)
        for g in r.json():
            name = g['name']
            if(name.lower().find(searchtext)<0 or g['cancelled']==True):
                continue
            print_line(g)
    except ValueError as err:
        print('Failed to list trainings: '+str(err))

def list_trainings(start_time=datetime.today(), end_time=datetime.today()):
    start_time = start_time.replace(hour=earliest,minute=0,second=0,microsecond=0)
    end_time = end_time.replace(hour=latest,minute=0,second=0,microsecond=0)
    r = get_trainings(start_time,end_time)
    try:
        be_verbose and verbose_message(r)
        for g in r.json():
            if(g["groupActivityProduct"]["id"] in exclude_list or g['cancelled']==True):
                continue
            print_line(g)
    except ValueError as err:
        print('Failed to list trainings: '+str(err))
    
def book(activity_id):
    access_token, customerid = brp_auth()
    headers = {
        'Content-Type':'application/json',
        'Authorization':'Bearer ' + access_token
    }
    params = {
        'groupActivity':activity_id
    }
    r = requests.post(base_url+'/customers/'+customerid+'/bookings/groupactivities',headers=headers,json=params)
    try:
        be_verbose and verbose_message(r)
    except ValueError as err:
        print('Failed to book: '+str(err))

def schedule(activity_id):
    headers = {
        'Content-Type':'application/json'
    }
    r = requests.get(url=base_url+f'/businessunits/1/groupactivities/{activity_id}',headers=headers)
    try:
        be_verbose and verbose_message(r)
        bookable_earliest = datetime.fromisoformat(r.json()['bookableEarliest'].replace('Z','+00:00')).astimezone()
        if(bookable_earliest < datetime.now().astimezone()):
            print(f'You can already book the grouptraining {activity_id}.')
            print(f'Use -b {activity_id}')
            return
        else:
            atcmd = f'echo ". venv/bin/activate && python3 bookbot.py -b {activity_id} -V && deactivate" | at '+ \
                bookable_earliest.time().isoformat(timespec='minutes') + ' ' + \
                    bookable_earliest.date().isoformat()
            print(atcmd)
            subprocess.run(atcmd,shell=True)
    except ValueError as err:
        print(f'Failed to schedule booking for {activity_id}: '+str(err))
    

parser = argparse.ArgumentParser(description='Interface with Gerdahallen booking system',
epilog='Without any arguments, %(prog)s lists the grouptrainings for today.')
parser.add_argument('--verbose','-V',help='show verbose messages from communication with backend at '+base_url,
action='store_true')
parser.add_argument('--date','-d',help='date or date range for search or listing, in format YYYY-MM-DD[:YYYY-MM-DD]',
dest='date',default=datetime.today().date().isoformat())
group = parser.add_mutually_exclusive_group(required=False)
group.add_argument('--find','-f',help='find available grouptrainings, case-insensitive',
dest='searchstring')
group.add_argument('--book','-b',help='try to book the grouptraining with activity_id',
dest='activity_id')
group.add_argument('--schedule','-s',help='schedule a booking attempt for earliest possible time',
dest='schedule_activity_id')

args = parser.parse_args()
be_verbose = args.verbose
slask = args.date.split(':')
start_date = slask[0]
end_date = len(slask)>1 and slask[1] or start_date

if (args.searchstring):
    search_trainings(searchtext=args.searchstring.lower(),
    start_time=datetime.fromisoformat(start_date),
    end_time=datetime.fromisoformat(end_date))
elif (args.activity_id):
    book(args.activity_id)
elif (args.schedule_activity_id):
    schedule(args.schedule_activity_id)
else:
    list_trainings(start_time=datetime.fromisoformat(start_date),
    end_time=datetime.fromisoformat(end_date))

