#!/bin/bash
test -d venv || python3 -m venv --system-site-packages venv
source venv/bin/activate
python -m pip install requests
deactivate
